# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2014, 2016, 2018, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-09 01:04+0000\n"
"PO-Revision-Date: 2023-02-09 13:35+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.2\n"

#: baloosearchrunner.cpp:65
#, kde-format
msgid "Open Containing Folder"
msgstr "De map openen waarin het zich bevindt"

#: baloosearchrunner.cpp:87
#, kde-format
msgid "Audios"
msgstr "Audios"

#: baloosearchrunner.cpp:88
#, kde-format
msgid "Images"
msgstr "Afbeeldingen"

#: baloosearchrunner.cpp:89
#, kde-format
msgid "Videos"
msgstr "Video's"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Spreadsheets"
msgstr "Rekenbladen"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Presentations"
msgstr "Presentaties"

#: baloosearchrunner.cpp:92
#, kde-format
msgid "Folders"
msgstr "Mappen"

#: baloosearchrunner.cpp:93
#, kde-format
msgid "Documents"
msgstr "Documenten"

#: baloosearchrunner.cpp:94
#, kde-format
msgid "Archives"
msgstr "Archieven"

#: baloosearchrunner.cpp:95
#, kde-format
msgid "Texts"
msgstr "Teksten"

#: baloosearchrunner.cpp:96
#, kde-format
msgid "Files"
msgstr "Bestanden"

#~ msgid "Search through files, emails and contacts"
#~ msgstr "Doorzoekt bestanden, e-mailberichten en contactpersonen"

#~ msgid "Email"
#~ msgstr "E-mail"
